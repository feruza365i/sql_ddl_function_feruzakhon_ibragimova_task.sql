CREATE TABLE film (
    film_id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255),
    category_id INT,
    rental_rate DECIMAL(5, 2),
    rental_duration INT,
    replacement_cost DECIMAL(5, 2),
    release_year INT,
    language_id INT
);

CREATE TABLE category (
    category_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE language (
    language_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE sale (
    sale_id INT AUTO_INCREMENT PRIMARY KEY,
    film_id INT,
    sale_date DATE,
    amount DECIMAL(8, 2)
);
INSERT INTO category (name) VALUES ('Action'), ('Comedy'), ('Drama');

INSERT INTO language (name) VALUES ('English'), ('Spanish'), ('Klingon');

-- Insert films
INSERT INTO film (title, category_id, rental_rate, rental_duration, replacement_cost, release_year, language_id)
VALUES ('Film 1', 1, 3.99, 5, 14.99, 2020, 1),
       ('Film 2', 2, 4.99, 4, 19.99, 2021, 2),
       ('Film 3', 3, 2.99, 7, 9.99, 2019, 1);

-- Insert sales
INSERT INTO sale (film_id, sale_date, amount)
VALUES (1, '2023-10-15', 10.50),
       (2, '2023-11-05', 15.75),
       (3, '2023-10-20', 8.25);
CREATE VIEW sales_revenue_by_category_qtr AS
SELECT c.name AS category, SUM(s.amount) AS total_revenue
FROM sale s
JOIN film f ON s.film_id = f.film_id
JOIN category c ON f.category_id = c.category_id
WHERE QUARTER(s.sale_date) = QUARTER(CURDATE()) AND YEAR(s.sale_date) = YEAR(CURDATE())
GROUP BY c.name;
DELIMITER //

CREATE PROCEDURE get_sales_revenue_by_category_qtr(IN current_quarter INT, OUT category_name VARCHAR(50), OUT total_revenue DECIMAL(8, 2))
BEGIN
    SELECT c.name, SUM(s.amount)
    INTO category_name, total_revenue
    FROM sale s
    JOIN film f ON s.film_id = f.film_id
    JOIN category c ON f.category_id = c.category_id
    WHERE QUARTER(s.sale_date) = current_quarter AND YEAR(s.sale_date) = YEAR(CURDATE())
    GROUP BY c.name;
END //

DELIMITER ;
DELIMITER //
CREATE PROCEDURE new_movie(movie_title VARCHAR(255))
BEGIN
    DECLARE new_film_id INT;
    DECLARE lang_exists INT;

    SELECT COUNT(*) INTO lang_exists FROM language WHERE name = 'Klingon';

    IF lang_exists > 0 THEN
        INSERT INTO film (title, category_id, rental_rate, rental_duration, replacement_cost, release_year, language_id)
        VALUES (movie_title, 1, 4.99, 3, 19.99, YEAR(CURDATE()), (SELECT language_id FROM language WHERE name = 'Klingon'));
    ELSE
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Language Klingon does not exist in the database.';
    END IF;
END //
DELIMITER ;
